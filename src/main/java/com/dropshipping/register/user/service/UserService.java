package com.dropshipping.register.user.service;

import com.dropshipping.register.user.repository.document.User;
import com.dropshipping.register.user.repository.document.dto.UserDto;
import com.dropshipping.register.user.service.exception.InvalidPassword;
import com.dropshipping.register.user.service.exception.UserAlreadyExists;
import com.dropshipping.register.user.service.exception.UserNotFoundException;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;

import java.util.Optional;

public interface UserService {
    String registerUser(UserDto userDto) throws UserNotFoundException, InternalException, InvalidPassword, UserAlreadyExists;

    Optional<User> findUserByName(String name);

    void deleteUser(User user);

    String login(final UserDto userDto) throws UserNotFoundException, InternalException, InvalidPassword;
}
