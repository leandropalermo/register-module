package com.dropshipping.register.user.service.impl;

import com.dropshipping.register.user.repository.UserRepository;
import com.dropshipping.register.user.repository.document.User;
import com.dropshipping.register.user.repository.document.dto.UserDto;
import com.dropshipping.register.user.service.UserService;
import com.dropshipping.register.user.service.exception.InvalidPassword;
import com.dropshipping.register.user.service.exception.UserAlreadyExists;
import com.dropshipping.register.user.service.exception.UserNotFoundException;
import com.dropshipping.register.user.service.feign.SecurityFeign;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private SecurityFeign securityFeign;
    private ConversionService conversionService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, SecurityFeign securityFeign,ConversionService conversionService) {
        this.userRepository = userRepository;
        this.securityFeign = securityFeign;
        this.conversionService = conversionService;
    }

    @Override
    public String registerUser(UserDto userDto) throws InternalException, InvalidPassword, UserAlreadyExists, UserNotFoundException {
        User user = conversionService.convert(userDto, User.class);
        Optional<User> userFound = findUserByName(userDto.getName());
        if (userFound.isPresent()) {
            throw new UserAlreadyExists("User already exists.");
        }
        String encryptedPassword = encryptPassword(userDto);
        user.setPassword(encryptedPassword);
        userRepository.save(user);
        try {
            return login(userDto);
        } catch (Exception e) {
            userRepository.delete(user);
            throw new InternalException(e.getMessage());
        }
    }

    @Override
    public String login(final UserDto userDto) throws UserNotFoundException, InternalException, InvalidPassword {
        Optional<User> userFound = findUserByName(userDto.getName());
        userFound.orElseThrow(() -> new UserNotFoundException("User " + userDto.getName() +" was not found"));
        String encryptedPassword = encryptPassword(userDto);
        userFound.filter(u -> u.getPassword().equals(encryptedPassword)).orElseThrow(() -> new InvalidPassword("Invalid Password"));
        Optional<String> token = securityFeign.token(userDto.getName());
        return token.orElseThrow(() -> new InternalException("Creating token error."));
    }

    @Override
    public Optional<User> findUserByName(String name) {
        return Optional.ofNullable(userRepository.findByUsername(name));
    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    private String encryptPassword(final UserDto userDto) {
        Optional<String> encryptedPassword = securityFeign.getPassword(userDto);
        encryptedPassword.orElseThrow(() -> new InternalException("Internal error"));
        return encryptedPassword.get();
    }
}
