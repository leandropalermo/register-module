package com.dropshipping.register.user.service.feign;

import com.dropshipping.register.user.repository.document.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@FeignClient(name = "security-module")
public interface SecurityFeign {

    @PostMapping("/users/token/{username}")
    Optional<String> token(@PathVariable(value = "username") String username);

    @PostMapping("/users/password")
    Optional<String> getPassword(@RequestBody UserDto userDto);
}
