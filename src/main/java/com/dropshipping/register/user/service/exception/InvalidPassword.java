package com.dropshipping.register.user.service.exception;

public class InvalidPassword extends Exception {

    public InvalidPassword(String message) {
        super(message);
    }
}
