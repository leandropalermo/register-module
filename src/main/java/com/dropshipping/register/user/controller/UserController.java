package com.dropshipping.register.user.controller;

import com.dropshipping.register.user.repository.document.User;
import com.dropshipping.register.user.repository.document.dto.UserDto;
import com.dropshipping.register.user.service.UserService;
import com.dropshipping.register.user.service.exception.InvalidPassword;
import com.dropshipping.register.user.service.exception.UserAlreadyExists;
import com.dropshipping.register.user.service.exception.UserNotFoundException;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/public/users")
public class    UserController {

    @Value( "${leandro}" )
    private String teste;

    @Autowired
    private UserService userService;

    @PostMapping
    public String registerUser(@RequestBody UserDto userDto) throws UserNotFoundException, InternalException, InvalidPassword, UserAlreadyExists {
        System.out.println(teste);
        return userService.registerUser(userDto);

    }

    @PostMapping("/login")
    public String login(@RequestBody UserDto userDto) throws UserNotFoundException, InternalException, InvalidPassword {
        return userService.login(userDto);
    }

    @GetMapping("/{userName}")
    public User findUserByName(@PathVariable("userName") String userName) {
        return userService.findUserByName(userName).orElse(User.builder().build());
    }

    @DeleteMapping
    public void deleteUser(@RequestBody User user) {
        userService.deleteUser(user);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({UserNotFoundException.class})
    private String userNotFound(UserNotFoundException e) {
        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler({UserAlreadyExists.class})
    private String userAlreadyExists(UserAlreadyExists e) {
        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({InvalidPassword.class})
    private String invalidPassword(InvalidPassword e) {
        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Exception.class})
    private String invalidPassword(Exception e) {
        String resp = "Unexpected error.";
        if (!StringUtils.isEmpty(e.getMessage())) {
            resp = e.getMessage();
        }
        return resp;
    }
}
