package com.dropshipping.register.user.repository.document;

import com.dropshipping.register.user.repository.document.dto.UserDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDtoToUserConverter implements Converter<UserDto, User> {
    @Override
    public User convert(UserDto userDto) {
        User user = User.builder()
                .username(userDto.getName())
                .password(userDto.getPassword())
                .build();
        return user;
    }
}
