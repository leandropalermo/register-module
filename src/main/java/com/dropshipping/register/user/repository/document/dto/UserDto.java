package com.dropshipping.register.user.repository.document.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable {
    private String id;
    private String name;
    private String password;
}
